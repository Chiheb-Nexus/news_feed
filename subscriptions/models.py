"""News feed subscriptions models"""
from datetime import datetime

from django.db import models


class Subscriptions(models.Model):
    """News feed subscriptions"""
    email = models.EmailField(verbose_name='Email', unique=True,)
    name = models.CharField(verbose_name='Name', max_length=100, default=None, blank=True)
    last_name = models.CharField(verbose_name='Last name', max_length=100, default=None, blank=True)
    subscription_date = models.DateTimeField(verbose_name='Subscription date', default=datetime.now)
    is_active = models.BooleanField(verbose_name='Is active', default=True)

    class Meta:
        verbose_name_plural = "Subscriptions"

    def __str__(self):
        return f'[{self.subscription_date}] {self.last_name} {self.name} <{self.email}>'
