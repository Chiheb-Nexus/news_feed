from django.views.generic.edit import FormView
from subscriptions import forms


class NewsFeedSubscriptionFormView(FormView):
    template_name = 'subscriptions/news_feed_subscription.html'
    form_class = forms.NewsFeedSubscriptionForm
    success_url = '/subscription/thanks/'

    def form_valid(self, form):
        if super().form_valid(form):
            form.save()
        return super().form_valid(form)
