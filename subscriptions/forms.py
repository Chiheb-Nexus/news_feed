"""Handling forms for Subscriptions app"""

from django.forms import ModelForm
from subscriptions import models


class NewsFeedSubscriptionForm(ModelForm):

    class Meta:
        model = models.Subscriptions
        fields = ('name', 'last_name', 'email')
