from django.contrib import admin
from subscriptions import models


@admin.register(models.Subscriptions)
class SubscriptionsAdmin(admin.ModelAdmin):
    """Subscriptions in admin panel"""
    pass
