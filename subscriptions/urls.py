"""Subscriptions app URLs"""

from django.urls import path
from django.views.generic import TemplateView

from subscriptions import views

urlpatterns = [
    path('', views.NewsFeedSubscriptionFormView.as_view()),
    path('thanks/', TemplateView.as_view(template_name="subscriptions/thanks.html"))
]
