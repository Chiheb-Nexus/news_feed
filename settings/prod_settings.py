from settings.base_settings import *  # noqa
from configparser import ConfigParser


parser = ConfigParser()
parser.read('credentials.ini')

SECRET_KEY = parser['app_configs_prod']['secret_key']
DEBUG = False
