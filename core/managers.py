"""User managers"""

from django.contrib.auth.base_user import BaseUserManager, AbstractBaseUser


class UserManager(BaseUserManager):
    """User manager"""
    use_in_migrations = True

    def _create_user(self, email: str, password: str, **extra_fields) -> AbstractBaseUser:
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('You must provide a non empty email')
        email = self.normalize_email(email)
        user: AbstractBaseUser = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields) -> AbstractBaseUser:
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields) -> AbstractBaseUser:
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)
