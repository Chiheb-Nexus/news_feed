# Testing example within Django
-----

### Setup

You need to add a `credentials.ini` file under `settings` folder and mainly (for now) add

```
[app_configs_dev]
secret_key=<PUT_YOUR_SEED_HERE>
```

### How to run the project locally

```bash
$> python manage.py runserver --settings=settings.dev_settings
```

## How to migrate your APP db

```bash
$> python manage.py makemigrations core subscriptions --settings=dev_settings
$> python manage.py migrate --settings=settings=dev_settings
```

## How to create a super user

```bash
$> python manage.py shell --settings=settings.dev_settings
$> from core import models
$> models.User.objects.create_user(email='<PUT_YOUR_EMAIL>', password='<PUT_YOUR_PASSWORD>', is_staff=True, is_superuser=True)
```